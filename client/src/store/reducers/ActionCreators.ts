import axios from "axios";

import {ICard} from "../../models/ICard";
import {todoSlice} from "./TodoSlice"
import {AppDispatch} from "../index";


export const fetchCards = () => async (dispatch: AppDispatch) => {
  try {
      dispatch(todoSlice.actions.cardsFetching());

      const response = await axios.get<ICard[]>('https://jsonplaceholder.typicode.com/todos');

      dispatch(todoSlice.actions.cardsFetchingSuccess(response.data));
  } catch (e) {
       dispatch(todoSlice.actions.cardsFetchingError('No load the cards'));
  }
}