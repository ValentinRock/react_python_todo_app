import {ICard} from "../../models/ICard";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";

interface TodoState {
    cards: ICard[];
    isLoading: boolean;
    error: string;
}

const initialState: TodoState = {
    cards: [],
    isLoading: false,
    error: ''
}

export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        cardsFetching(state) {
            state.isLoading = true;
        },

        cardsFetchingSuccess(state, action: PayloadAction<ICard[]>) {
            state.isLoading = false;
            state.error = '';
            state.cards = action.payload;
        },

        cardsFetchingError(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
        }
    }
})


export default todoSlice.reducer;