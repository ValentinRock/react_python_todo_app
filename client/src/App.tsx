import React, {useEffect} from 'react';
import {useAppDispatch, useAppSelector} from "./hooks/redux";
import {fetchCards} from "./store/reducers/ActionCreators";

const App = () => {
    const dispatch = useAppDispatch();
    const {cards, isLoading, error} = useAppSelector(state => state.todoReducer);

    useEffect(() => {
        dispatch(fetchCards())
    }, []);

    if (isLoading) {
        return <h1>Loading...</h1>
    }

    if (error) {
        return <h1>{error}</h1>
    }

    return (
        <div>
            {cards.map(card =>
                <div key={card.id}>
                    <h2>
                        {card.title}
                    </h2>

                    <p>
                        User Id is {card.userId}
                    </p>

                    <input type="checkbox" readOnly={true} checked={card.completed}/>
                </div>
            )}
        </div>
    );
};

export default App;