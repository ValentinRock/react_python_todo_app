
# React & Django ToDo App

Creative todo app


# Usage

## To launch the Frontend

```bash
  cd ./client
  npm i
  npm start
```
## Tech Stack

**Client:** React & TypeScript & Redux ToolKit

**Server:** Python Django REST Framework


## Requirements

### Frontend
NodeJS v16+
NPM v6+